package cmd

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	apihandlers "oricom.io/security/api/v1/handlers"
	"oricom.io/security/web/handlers"
)

// Server has router and db instances
type Server struct {
	Router *mux.Router

	//handlers
	handlersUsersAPI *apihandlers.HandlerUsers
}

// Run the app on it's router
func (a *Server) Run() {
	a.initHandlers()
	a.Router.Use(CommomMiddleware)

	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
	})

	log.Printf("listening do port: %s", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), corsOpts.Handler(a.Router)))
}

func (a *Server) initHandlers() {
	//web
	securityHandler := handlers.NewSecurityHandler()
	securityHandler.Init(a.Router)

	//api
	a.handlersUsersAPI = apihandlers.NewHandlerUsers(a.Router)
}

// NewServer creates a new instance of server
func NewServer() *Server {
	return &Server{
		Router: mux.NewRouter(),
	}
}
