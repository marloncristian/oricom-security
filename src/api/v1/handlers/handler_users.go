package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/marloncristian/oricom-goframework/database"
	"github.com/marloncristian/oricom-goframework/web/authentication"
	"gopkg.in/go-playground/validator.v9"
	"oricom.io/security/domain"
)

// HandlerUsers handler for users api
type HandlerUsers struct {
}

// NewHandlerUsers the controller configurations
func NewHandlerUsers(router *mux.Router) *HandlerUsers {
	handler := &HandlerUsers{}

	//subroutes
	v1 := router.PathPrefix("/v1").Subrouter()
	me := v1.PathPrefix("/me").Subrouter()

	//gets
	v1.HandleFunc("/users", authentication.AuthenticateRole("admin", handler.getUsers)).Methods("GET")
	v1.HandleFunc("/users/{id}", authentication.AuthenticateRole("admin", handler.getUser)).Methods("GET")
	v1.HandleFunc("/users/{id}", authentication.AuthenticateRole("admin", handler.patchUser)).Methods("PATCH")

	// v1/me/users (me)
	me.HandleFunc("/users", authentication.Authenticate(handler.getMeUser)).Methods("GET")
	me.HandleFunc("/users", authentication.Authenticate(handler.patchUser)).Methods("PATCH")

	return handler
}

// getUsers gets all users from database
func (handler HandlerUsers) getUsers(w http.ResponseWriter, r *http.Request) {
	qrOffset := r.URL.Query().Get("_offset")
	qrLimit := r.URL.Query().Get("_limit")

	offset := int64(0)
	if len(qrOffset) > 0 {
		offsetPar, err := strconv.ParseInt(qrOffset, 10, 64)
		if err == nil {
			offset = offsetPar
		}
	}

	limit := int64(100)
	if len(qrLimit) > 0 {
		limitPar, err := strconv.ParseInt(qrLimit, 10, 64)
		if err == nil {
			limit = limitPar
		}
	}

	var filter *domain.UsersFilter
	if r.URL.Query().Get("name") != "" {
		filter = &domain.UsersFilter{
			Name: r.URL.Query().Get("name"),
		}
	}

	svc := domain.NewServiceUser()
	usrs, err := svc.GetWithSkipLimit(offset, limit, filter)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if len(usrs) == 0 {
		http.Error(w, "", http.StatusNoContent)
		return
	}

	response := []domain.UsersGetModel{}
	for _, item := range usrs {
		response = append(response, domain.UsersGetModel{
			ID:        item.ID.Hex(),
			Name:      item.Name,
			Email:     item.Email,
			Role:      item.Role,
			Picture:   item.Picture,
			CreatedAt: item.CreatedAt,
		})
	}

	json.NewEncoder(w).Encode(response)
}

// getUser returns the data for a specific user
func (handler HandlerUsers) getUser(w http.ResponseWriter, r *http.Request) {
	usrSvc := domain.NewServiceUser()
	usr, err := usrSvc.GetByHexID(mux.Vars(r)["id"])
	if _, ok := err.(database.EntityNotFound); ok {
		http.Error(w, "", http.StatusNotFound)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(domain.UsersGetModel{
		ID:        usr.ID.Hex(),
		Name:      usr.Name,
		Email:     usr.Email,
		Role:      usr.Role,
		Picture:   usr.Picture,
		CreatedAt: usr.CreatedAt,
	})
}

// getUser returns the data for a specific user
func (handler HandlerUsers) getMeUser(w http.ResponseWriter, r *http.Request) {
	sub, err := authentication.GetTokenSubFromHeader(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	usrSvc := domain.NewServiceUser()
	usr, err := usrSvc.GetByHexID(sub)
	if _, ok := err.(database.EntityNotFound); ok {
		http.Error(w, "", http.StatusNotFound)
		return

	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(domain.UsersGetModel{
		Name:      usr.Name,
		Email:     usr.Email,
		Role:      usr.Role,
		Picture:   usr.Picture,
		CreatedAt: usr.CreatedAt,
	})
}

// patchUser executes a partial update of users
func (handler HandlerUsers) patchUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	tok, err := authentication.ParseTokenFromHeader(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	isAdmin := authentication.CheckTokenRoleFromHeader("admin", r)
	if !isAdmin && tok["sub"].(string) != vars["id"] {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	usrSvc := domain.NewServiceUser()
	_, err = usrSvc.GetByHexID(vars["id"])
	if _, ok := err.(database.EntityNotFound); ok {
		http.Error(w, "", http.StatusNotFound)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	dec := json.NewDecoder(r.Body)
	pat := domain.UserPatchModel{}
	if err := dec.Decode(&pat); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	err = validate.Struct(&pat)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = usrSvc.Update(vars["id"], map[string]interface{}{
		"name": pat.Name,
	})
	if _, ok := err.(domain.ElementNotModified); ok {
		http.Error(w, err.Error(), http.StatusNotModified)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	handler.getUser(w, r)
}
