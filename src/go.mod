module oricom.io/security

require (
	github.com/Masterminds/semver v1.4.2 // indirect
	github.com/Masterminds/vcs v1.12.0 // indirect
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf // indirect
	github.com/astaxie/beego v1.11.1 // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/casbin/casbin v1.8.0 // indirect
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/coreos/go-oidc v2.1.0+incompatible
	github.com/couchbase/go-couchbase v0.0.0-20190117181324-d904413d884d // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/golang/dep v0.5.0 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.0.2 // indirect
	github.com/jmank88/nuts v0.3.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/marloncristian/oricom-goframework v0.1.0
	github.com/nightlyone/lockfile v0.0.0-20180618180623-0ad87eef1443 // indirect
	github.com/onsi/ginkgo v1.11.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/rs/cors v1.7.0
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sdboyer/constext v0.0.0-20170321163424-836a14457353 // indirect
	github.com/syndtr/goleveldb v0.0.0-20181128100959-b001fa50d6b2 // indirect
	github.com/thedevsaddam/govalidator v1.9.5 // indirect
	github.com/tools/godep v0.0.0-20180126220526-ce0bfadeb516 // indirect
	github.com/wendal/errors v0.0.0-20181209125328-7f31f4b264ec // indirect
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/oauth2.v3 v3.12.0 // indirect
	gopkg.in/square/go-jose.v2 v2.4.1 // indirect
)

go 1.13
