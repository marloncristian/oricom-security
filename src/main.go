package main

import (
	"log"
	"os"

	"oricom.io/security/cmd"
	"oricom.io/security/utils"

	"github.com/marloncristian/oricom-goframework/database"
	"github.com/marloncristian/oricom-goframework/web/authentication"
)

// main rest api endpoint
func main() {

	//initializes the bases for the aplication to start
	database.Initialize(utils.ConfigurationInstance.GetServerURL(), utils.ConfigurationInstance.DatabaseName)
	authentication.Initialize(utils.ConfigurationInstance.Security.Secret)

	log.SetOutput(os.Stdout)
	log.Print("initializing server...")

	//initializes the api and web server
	server := cmd.NewServer()
	server.Run()
}
