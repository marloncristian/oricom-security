package domain

import (
	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// ServiceUser service responsable for security iteraction
type ServiceUser struct {
	serviceBase database.ServiceBase
}

func (service ServiceUser) getPagedRaw(pageIndex int64) (UsersPagedModel, error) {
	res := []UsersGetModel{}

	err := service.serviceBase.GetAllWithSkipLimit(&res, pageIndex*PageSize, PageSize)
	if err != nil {
		return UsersPagedModel{}, err
	}

	cnt, err := service.serviceBase.CountAll()
	if err != nil {
		return UsersPagedModel{}, err
	}

	return UsersPagedModel{
		PageCount: cnt / PageSize,
		PageIndex: pageIndex,
		Users:     res,
	}, nil
}

// GetWithSkipLimit returns a sub collection of users based on input parameters
func (service ServiceUser) GetWithSkipLimit(offset int64, limit int64, filter *UsersFilter) ([]UserEntity, error) {
	res := []UserEntity{}

	query := bson.M{}
	if filter != nil && filter.Name != "" {
		query["name"] = bson.M{"$regex": primitive.Regex{Pattern: filter.Name, Options: "i"}}
	}

	err := service.serviceBase.GetWithSkipLimit(query, &res, offset, limit)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// GetByEmail gets the user by email
func (service ServiceUser) GetByEmail(email string) (UserEntity, error) {
	res := UserEntity{}
	err := service.serviceBase.GetOne(bson.M{"email": email}, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// GetByEmailAndPassword gets the user by email
func (service ServiceUser) GetByEmailAndPassword(email string, password string) (UserEntity, error) {
	res := UserEntity{}
	err := service.serviceBase.GetOne(bson.M{"email": email, "password": password}, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// GetByObjID gets the user by id
func (service ServiceUser) GetByObjID(id primitive.ObjectID) (UserEntity, error) {
	res := UserEntity{}
	err := service.serviceBase.GetByObjID(id, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// GetByHexID gets the user by string id
func (service ServiceUser) GetByHexID(id string) (UserEntity, error) {
	res := UserEntity{}
	err := service.serviceBase.GetByHexID(id, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// Create creates a new user entry
func (service ServiceUser) Create(user UserEntity) (primitive.ObjectID, error) {
	id, err := service.serviceBase.InsertOne(user)
	if err != nil {
		return primitive.ObjectID{}, err
	}
	return id, nil
}

// Update updates the user properties
func (service ServiceUser) Update(id string, values map[string]interface{}) error {
	obj, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}

	usr, getErr := service.GetByHexID(id)
	if getErr != nil {
		return getErr
	}

	_, namOk := values["name"]
	if !namOk || values["name"].(string) == usr.Name {
		return ElementNotModified{}
	}

	if err := service.serviceBase.UpdateOne(obj, values, nil); err != nil {
		return err
	}
	return nil
}

// UpdateProviders atualiza os providers de uma instancia
func (service ServiceUser) UpdateProviders(id primitive.ObjectID, providers map[string]UserProviderEntity) error {
	result := UserEntity{}
	err := service.serviceBase.UpdateOne(id, map[string]interface{}{
		"providers": providers,
	}, &result)
	if err != nil {
		return err
	}
	return nil
}

// NewServiceUser creats a new service user instance
func NewServiceUser() ServiceUser {
	return ServiceUser{
		serviceBase: database.NewServiceBase("users"),
	}
}
