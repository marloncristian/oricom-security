package domain

import (
	"github.com/marloncristian/oricom-goframework/database"
)

// ServiceSecuritySettings security settings service
type ServiceSecuritySettings struct {
	serviceBase database.ServiceBase
}

// GetSettings gets the current security settings
func (service ServiceSecuritySettings) GetSettings() (SecuritySettingsEntity, error) {
	res := SecuritySettingsEntity{}
	err := service.serviceBase.GetOne(nil, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// NewServiceSecuritySettings creates a new security settings service
func NewServiceSecuritySettings() ServiceSecuritySettings {
	return ServiceSecuritySettings{
		serviceBase: database.NewServiceBase("securitysettings"),
	}
}
