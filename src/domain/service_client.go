package domain

import (
	"github.com/marloncristian/oricom-goframework/database"
	"go.mongodb.org/mongo-driver/bson"
)

// ServiceClient service responsable for security iteraction
type ServiceClient struct {
	serviceBase database.ServiceBase
}

// GetByClientIDAndClientSecret gets the cilent by client id and client secret
func (service ServiceClient) GetByClientIDAndClientSecret(clientID string, clientSecret string) (ClientEntity, error) {
	res := ClientEntity{}
	err := service.serviceBase.GetOne(bson.M{"clientid": clientID, "clientsecret": clientSecret}, &res)
	if err != nil {
		return res, err
	}
	return res, nil
}

// NewServiceClient creates a new cliente service
func NewServiceClient() ServiceClient {
	return ServiceClient{
		serviceBase: database.NewServiceBase("clients"),
	}
}
