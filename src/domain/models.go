package domain

import "time"

//UsersPagedModel model for user
type UsersPagedModel struct {
	Users     []UsersGetModel
	PageIndex int64
	PageCount int64
}

// UsersFilter model for users filtering
type UsersFilter struct {
	Name string
}

// UsersGetModel model for retrieving simple users information
type UsersGetModel struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Picture   string    `json:"picture"`
	Role      []string  `json:"role"`
	CreatedAt time.Time `json:"createdat"`
}

// UserPatchModel model for updating users information
type UserPatchModel struct {
	Name string `validate:"required"`
}
