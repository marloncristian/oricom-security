package domain

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// UserEntity User entity structure
type UserEntity struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name      string
	Email     string
	Picture   string
	Role      []string
	CreatedAt time.Time
	Password  string
	Providers map[string]UserProviderEntity
}

// UserProviderEntity entity structure
type UserProviderEntity struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Subject      string
	AccessToken  string
	RefreshToken string
}

// SecuritySettingsEntity configuration settings entity
type SecuritySettingsEntity struct {
	ID        primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Redirects map[string]string
}

// ClientEntity client entity for application integration
type ClientEntity struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Name         string             `json:"name"`
	ClientID     string             `json:"clientid"`
	ClientSecret string             `json:"clientsecret"`
	Active       bool               `json:"active"`
}
