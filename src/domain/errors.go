package domain

//ElementNotFound persistence error for zero elements result
type ElementNotFound struct {
}

func (err ElementNotFound) Error() string {
	return "Element not found"
}

//ElementNotModified persistence error for non modified updates
type ElementNotModified struct {
}

func (err ElementNotModified) Error() string {
	return "Element not found"
}
