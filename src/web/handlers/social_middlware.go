package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
)

//SocialMiddleware interface for social interaction
type SocialMiddleware interface {
	Init(router *mux.Router)
	authenticate(w http.ResponseWriter, r *http.Request)
}
