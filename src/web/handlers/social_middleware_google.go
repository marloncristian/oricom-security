package handlers

import (
	"log"
	"time"
	"errors"
	"net/http"
	"encoding/json"

	"oricom.io/security/domain"
	"github.com/coreos/go-oidc"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"github.com/gorilla/mux"
	"github.com/marloncristian/oricom-goframework/web/authentication"
	"github.com/marloncristian/oricom-goframework/database"
)

// SocialMiddlewareGoogle google middleware
type SocialMiddlewareGoogle struct {
	provider 		*oidc.Provider
}

// UserClaims user claims returned by provider
type UserClaims struct {
	Name              string
	GivenName         string
	FamilyName        string
	PreferredUsername string
	Picture           string
	EmailVerified     *bool
}

type tokenJSON struct {
	AccessToken  string         `json:"access_token"`
	TokenType    string         `json:"token_type"`
	RefreshToken string			`json:"refresh_token"`
}

type authenticateResponse struct {
	AccessToken string						`json:"access_token"`
	User 		authenticateUserResponse 	`json:"user"`
}
type authenticateUserResponse struct {
	ID	 		string		`json:"id"`
	Name 		string 		`json:"name"`
	Role 		[]string	`json:"role"`
	Picture		string		`json:"picture_url"`
}

// Init initializes the middleware
func (mid SocialMiddlewareGoogle) Init(router *mux.Router) {
	router.HandleFunc("/auth/google/authenticate", mid.authenticate)
}

func (mid SocialMiddlewareGoogle) authenticate(w http.ResponseWriter, r *http.Request) {

	tok := tokenJSON{}
	err := json.NewDecoder(r.Body).Decode(&tok)
	if err != nil {
		http.Error(w, "Error parsing userinfo response: " + err.Error(), http.StatusInternalServerError)
		return
	}

	otok := oauth2.Token {
		AccessToken:  tok.AccessToken,
		TokenType:    tok.TokenType,
		RefreshToken: tok.RefreshToken,
	}
	
	userInfo, err := mid.provider.UserInfo(context.Background(), oauth2.StaticTokenSource(&otok))
	if err != nil {
		http.Error(w, "Failed to get userinfo: " + err.Error(), http.StatusInternalServerError)
		return
	}

	usrClaims := UserClaims {}
	if err := userInfo.Claims(&usrClaims); err != nil {
		http.Error(w, "Error parsing user information: " + err.Error(), http.StatusBadRequest)
		return
	}
	
	usr, err := mid.getUser(userInfo, &otok, usrClaims)
	if err != nil {
		http.Error(w, "Error parsing user information: " + err.Error(), http.StatusBadRequest)
		return
	}

	token, err := authentication.CreateToken(usr.ID.Hex(), usr.Name, usr.Role)
	if err != nil {
		http.Error(w, "Error generating token: " + err.Error(), http.StatusBadRequest)
		return
	}

	res := authenticateResponse{
		AccessToken : token,
		User : authenticateUserResponse{
			ID : usr.ID.Hex(),
			Name : usr.Name,
			Role : usr.Role,
			Picture : usr.Picture,
		},
	}
	json.NewEncoder(w).Encode(res)
}

// getUser resolves de user instance
func (mid SocialMiddlewareGoogle) getUser(userInfo *oidc.UserInfo, oAuth2Token *oauth2.Token, userClaims UserClaims) (domain.UserEntity, error) {
	svc := domain.NewServiceUser()

	user, err := svc.GetByEmail(userInfo.Email)
	if _, ok := err.(database.EntityNotFound); ok {

		id, err := svc.Create(domain.UserEntity{
			Name : userClaims.Name,
			Email : userInfo.Email,
			Picture : userClaims.Picture,
			Role : []string{"user"},
			CreatedAt : time.Now(),
			Providers : map[string]domain.UserProviderEntity {
				"google" : domain.UserProviderEntity {
					Subject : userInfo.Subject,
					AccessToken : oAuth2Token.AccessToken,
					RefreshToken : oAuth2Token.RefreshToken,
				},
			},
		}); 
		if err != nil {
			return domain.UserEntity{}, err
		}

		user, err = svc.GetByObjID(id)
		if _, ok := err.(database.EntityNotFound); ok {
			return domain.UserEntity{}, errors.New("Error in previous user creation")
		} else if err != nil {
			return domain.UserEntity{}, err
		}

	} else if err != nil {
		return domain.UserEntity{}, err
	} else if _, ok := user.Providers["google"]; !ok {

		user.Providers["google"] = domain.UserProviderEntity{
			Subject: userInfo.Subject,
			AccessToken: oAuth2Token.AccessToken,
			RefreshToken: oAuth2Token.RefreshToken,
		}

		if err := svc.UpdateProviders(user.ID, user.Providers); err != nil {
			return domain.UserEntity{}, err
		}
	}

	return user, nil
}

// NewSocialMiddlewareGoogle creates a new social middleware for google
func NewSocialMiddlewareGoogle() SocialMiddlewareGoogle {
	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, "https://accounts.google.com")
	if err != nil {
		log.Fatal(err)
	}

	return SocialMiddlewareGoogle{
		provider : provider,
	}
}
