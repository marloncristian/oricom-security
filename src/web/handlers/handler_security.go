package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"
	"github.com/marloncristian/oricom-goframework/web/authentication"
	"oricom.io/security/domain"
)

// SecurityHandler controller for security procedures
type SecurityHandler struct {
	middlewares map[string]SocialMiddleware
}

// Init the controller configurations
func (controller SecurityHandler) Init(router *mux.Router) {
	//initialize social middleware
	for _, midd := range controller.middlewares {
		midd.Init(router)
	}

	//auth hanlder
	router.HandleFunc("/auth", controller.auth).Methods("POST")
}

// dedicate default auth route
func (controller SecurityHandler) auth(w http.ResponseWriter, r *http.Request) {
	bytes, errReading := ioutil.ReadAll(r.Body)
	if errReading != nil {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	body := string(bytes)
	values, errParsing := url.ParseQuery(body)
	if errParsing != nil {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	if values.Get("grant_type") != "password" {
		http.Error(w, "Unsupported grant type", http.StatusBadRequest)
		return
	}
	if len(values.Get("username")) == 0 || len(values.Get("password")) == 0 {
		http.Error(w, "Invalid credentials", http.StatusBadRequest)
		return
	}
	if len(values.Get("client_id")) == 0 || len(values.Get("client_secret")) == 0 {
		http.Error(w, "Invalid client credentials", http.StatusBadRequest)
		return
	}
	clientService := domain.NewServiceClient()
	_, errClient := clientService.GetByClientIDAndClientSecret(strings.TrimSpace(values.Get("client_id")), strings.TrimSpace(values.Get("client_secret")))
	if errClient != nil {
		http.Error(w, "Invalid client credentials", http.StatusBadRequest)
		return
	}
	userService := domain.NewServiceUser()
	usr, errUser := userService.GetByEmailAndPassword(strings.TrimSpace(values.Get("username")), strings.TrimSpace(values.Get("password")))
	if errUser != nil {
		http.Error(w, "Invalid credentials", http.StatusBadRequest)
		return
	}

	token, err := authentication.CreateToken(usr.ID.Hex(), usr.Name, usr.Role)
	if err != nil {
		http.Error(w, "Error generating token: "+err.Error(), http.StatusBadRequest)
		return
	}

	res := authenticateResponse{
		AccessToken: token,
		User: authenticateUserResponse{
			ID:      usr.ID.Hex(),
			Name:    usr.Name,
			Role:    usr.Role,
			Picture: usr.Picture,
		},
	}
	json.NewEncoder(w).Encode(res)
}

// NewSecurityHandler New security controller
func NewSecurityHandler() *SecurityHandler {
	return &SecurityHandler{
		middlewares: map[string]SocialMiddleware{
			"google": NewSocialMiddlewareGoogle(),
		},
	}
}
