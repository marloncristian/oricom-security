package utils

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis"
)

type StateController struct {
	address  string
	password string
	database int
}

type StateFragment struct {
	Provider    string
	GeneratedAt time.Time
	Module      string
}

//Set creates a new session key
func (controller StateController) Set(key string, value StateFragment) error {

	client := redis.NewClient(&redis.Options{
		Addr:     ConfigurationInstance.Redis.Address,
		Password: ConfigurationInstance.Redis.Password,
		DB:       ConfigurationInstance.Redis.Database,
	})

	mValue, err := json.Marshal(value)
	if err != nil {
		return err
	}

	if err := client.Set(key, string(mValue), 0).Err(); err != nil {
		return err
	}

	return nil
}

//Get returns an exising session key
func (controller StateController) Get(key string) (StateFragment, error) {

	client := redis.NewClient(&redis.Options{
		Addr:     ConfigurationInstance.Redis.Address,
		Password: ConfigurationInstance.Redis.Password,
		DB:       ConfigurationInstance.Redis.Database,
	})

	val, err := client.Get(key).Result()
	if err != nil {
		return StateFragment{}, err
	}

	stFrag := StateFragment{}
	if err := json.Unmarshal([]byte(val), &stFrag); err != nil {
		return StateFragment{}, err
	}

	return stFrag, nil
}

//Delete returns an exising session key
func (controller StateController) Delete(key string) error {

	client := redis.NewClient(&redis.Options{
		Addr:     ConfigurationInstance.Redis.Address,
		Password: ConfigurationInstance.Redis.Password,
		DB:       ConfigurationInstance.Redis.Database,
	})

	_, err := client.Del(key).Result()
	if err != nil {
		return err
	}

	return nil
}
