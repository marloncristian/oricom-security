package utils

import (
	"encoding/json"
	"fmt"
	"os"
)

// Configuration structure
type Configuration struct {
	ServerURL        string
	ServerPort       string
	DatabaseName     string
	DatabaseUsername string
	DatabasePassword string
	Port             string
	Redis            RedisConfiguration
	Security         SecurityConfiguration
}

//RedisConfiguration redis configurations struct
type RedisConfiguration struct {
	Address  string
	Password string
	Database int
}

// SecurityConfiguration security configuration struct
type SecurityConfiguration struct {
	Secret string
}

// GetServerURL returns the server url
func (configuration Configuration) GetServerURL() string {
	if configuration.DatabaseUsername != "" && configuration.DatabasePassword != "" {
		return fmt.Sprintf("mongodb://%s:%s@%s:%s/%s?retryWrites=false",
			configuration.DatabaseUsername,
			configuration.DatabasePassword,
			configuration.ServerURL,
			configuration.ServerPort,
			configuration.DatabaseName,
		)
	}

	return fmt.Sprintf("mongodb://%s:%s",
		configuration.ServerURL,
		configuration.ServerPort)
}

// Load returns a single instance of configuration
func (configuration *Configuration) Load() error {

	envVar := os.Getenv("ORI_ENV")
	buildVar := map[bool]string{true: "development", false: envVar}[envVar == ""]

	file, err := os.Open(fmt.Sprintf("settings.%s.json", buildVar))
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)
	if err := decoder.Decode(configuration); err != nil {
		return err
	}

	if err := file.Close(); err != nil {
		return err
	}

	return nil
}
