package utils

import (
	"github.com/marloncristian/oricom-goframework/core"
)

var (

	//ConfigurationInstance instance for configurations environment
	ConfigurationInstance Configuration

	//StateControllerInstance instance for state controller
	StateControllerInstance StateController
)

func init() {

	ConfigurationInstance = Configuration{}
	core.ParseConfiguration(&ConfigurationInstance)

	StateControllerInstance = StateController{
		address:  ConfigurationInstance.Redis.Address,
		password: ConfigurationInstance.Redis.Password,
		database: ConfigurationInstance.Redis.Database,
	}
}
